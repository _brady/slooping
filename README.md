# Slooping
> Sleeping Wooting - Turn off LEDs of your Wooting keyboard when you don't need them.

Script to run in the background and turn the LEDs of your Wooting keyboard off if you are inactive or lock your computer. By default the LEDs will be turned of after 20 minutes of inactivity.

The script uses the Wooting RGB SDK to control the LEDs on your keyboard.
For those that just want to run it, you can download a compiled application [here](/uploads/4e45c4e1c69eb9221d56d6b4964e1524/Slooping.exe) with no further dependencies, otherwise refer to [Installation](#installation).

## Installation

Currently only works on Windows with 64bit Python 3:

To get started clone this project, download or compile the Wooting SDK DLL and put it in /libs folder.

```sh
git clone --recursive https://gitlab.com/_brady/slooping.git
```
For the Wooting SDK DLLs refer to:
* RGB SDK: https://github.com/WootingKb/wooting-rgb-sdk

## Usage example

Just run ``slooping.py <delay in minutes>`` you can then minimize the console window, the default delay is 20 minutes.

For the executable you can just run it with default settings by double clicking or start it with a argument to change the delay by running ``slooping.exe <delay in minutes>`` in console.
## Release History

* 1.0
    * Release

## Meta

[Brady](https://gitlab.com/_brady/) – Contact me on the Wooting [Discord server](https://discordapp.com/invite/rNghtgw)

Distributed under the MIT license. See [LICENSE](LICENSE) for more information.