from argparse import ArgumentParser
from ctypes import Structure, windll, c_uint, sizeof, byref
from time import sleep

from woopy.woopy import Wooting

ACTIVE = True


class LASTINPUTINFO(Structure):
    _fields_ = [
        ('cbSize', c_uint),
        ('dwTime', c_uint),
    ]


def get_idle_duration() -> float:
    """
    Get system wide idle duration in seconds.
    :return: Duration in seconds.
    """

    lastInputInfo = LASTINPUTINFO()
    lastInputInfo.cbSize = sizeof(lastInputInfo)
    windll.user32.GetLastInputInfo(byref(lastInputInfo))
    inactive_ms = windll.kernel32.GetTickCount() - lastInputInfo.dwTime
    return inactive_ms / 1000


def get_user_locked() -> bool:
    """
    Return whether the user account is locked.
    """

    return not bool(windll.user32.OpenInputDesktop(0, 0, 0))


def check_activity(keyboard: Wooting, delay: int):
    """
    Check activity and turn of RGB if user is inactive, turn them on if user is active.

    :param keyboard: Wooting keyboard
    :param delay: Delay in seconds after which user is inactive.
    """

    global ACTIVE
    if ACTIVE:
        if get_user_locked() or get_idle_duration() > delay:
            keyboard.set_array_full([0] * 378)
            keyboard.update()
            ACTIVE = False
            print('LEDs turned off.')
            sleep(1)
    else:
        if get_idle_duration() < 1.5:
            keyboard.reset()
            ACTIVE = True
            print('LEDs turned on.')
            sleep(10)  # to keep LEDs on for at least 10 seconds, useful if screen is locked but user is active


if __name__ == '__main__':
    parser = ArgumentParser(description='Turn off RGB after inactivity.')
    parser.add_argument('delay', nargs='?', type=int, default=20, help='Time in minutes (default 20).')
    args = parser.parse_args()

    delay = max(1, args.delay) * 60  # minutes to seconds

    with Wooting(rgb_path='libs/wooting-rgb-sdk.dll') as kbd:
        if kbd.connected():
            print('Program is running ... (close with Strg+C)')
            while True:
                check_activity(kbd, delay)
                sleep(1)
        else:
            print('No Wooting keyboard found. :(')
